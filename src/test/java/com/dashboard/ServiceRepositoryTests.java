// package com.dashboard;

// import com.dashboard.entity.EntityService;
// import com.dashboard.repository.ServiceRepository;
// import org.junit.jupiter.api.Test;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
// import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
// import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
// import org.springframework.test.annotation.Rollback;

// import static org.assertj.core.api.Assertions.assertThat;

// @DataJpaTest
// @AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
// @Rollback(false)
// public class ServiceRepositoryTests {
//     @Autowired
//     private ServiceRepository serviceRepository;

//     @Autowired
//     private TestEntityManager entityManager;

//     @Test
//     public void testCreateService() {
//         EntityService service = new EntityService();

//         service.setConnectUrl("testConnectUrl");
//         service.setDefaultWebsite("testDefaultWebsite");
//         service.setDescription("testDescription");
//         service.setIcon("testIcon");
//         service.setPageReference("testPageReference");
//         service.setName("gitlab");

//         EntityService savedService = serviceRepository.save(service);

//         EntityService existService = entityManager.find(EntityService.class, savedService.getId());

//         assertThat(existService.getName()).isEqualTo(service.getName());
//     }
// }
