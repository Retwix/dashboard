// package com.dashboard;

// import com.dashboard.entity.User;
// import com.dashboard.repository.UserRepository;
// import org.junit.jupiter.api.Test;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
// import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
// import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
// import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
// import org.springframework.test.annotation.Rollback;

// import static org.assertj.core.api.Assertions.assertThat;

// @DataJpaTest
// @AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
// @Rollback(false)
// public class UserRepositoryTests {
//     @Autowired
//     private UserRepository userRepository;

//     @Autowired
//     private TestEntityManager entityManager;

//     @Test
//     public void testCreateUser() {
//         User user = new User();

//         user.setEmail("james.blake@lossantosbank.com");
//         user.setUsername("JamesBlake");

//         BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//         String encodedPassword = encoder.encode("12345");
//         user.setPassword(encodedPassword);
//         user.setAvatar("https://avatars.dicebear.com/api/gridy/JamesBlake.svg");
//         user.setAdmin(true);

//         User savedUser = userRepository.save(user);

//         User existUser = entityManager.find(User.class, savedUser.getId());

//         assertThat(existUser.getEmail()).isEqualTo(user.getEmail());
//     }

//     @Test
//     public void testFindUserByEmail() {
//         String email = "jeremy.dutel@icloud.com";

//         User user = userRepository.findByEmail(email);

//         assertThat(user).isNotNull();
//     }

//     @Test
//     public void testFindUserByUsername() {
//         String username = "jeremydutel";

//         User user = userRepository.findByUsername(username);

//         assertThat(user).isNotNull();
//     }
// }
