// package com.dashboard;

// import com.dashboard.entity.Widget;
// import com.dashboard.repository.WidgetRepository;
// import org.junit.jupiter.api.Test;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
// import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
// import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
// import org.springframework.test.annotation.Rollback;

// import static org.assertj.core.api.Assertions.assertThat;

// @DataJpaTest
// @AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
// @Rollback(false)
// public class WidgetRepositoryTests {
//     @Autowired
//     private WidgetRepository widgetRepository;

//     @Autowired
//     private TestEntityManager entityManager;

//     @Test
//     public void testCreateWidget() {
//         Widget widget = new Widget();

//         widget.setServiceId(1);
//         widget.setPage("testPage");
//         widget.setConfig("testConfig");
//         widget.setName("testName");
//         widget.setUrlApiRequest("testUrlApiRequest");
//         widget.setTypeApiRequest("testTypeApiRequest");

//         Widget savedWidget = widgetRepository.save(widget);

//         Widget existWidget = entityManager.find(Widget.class, savedWidget.getId());

//         assertThat(existWidget.getId()).isEqualTo(widget.getId());
//     }
// }
