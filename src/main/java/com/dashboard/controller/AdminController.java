package com.dashboard.controller;

import com.dashboard.entity.User;
import com.dashboard.security.CustomUserDetails;
import com.dashboard.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class AdminController {

    @Autowired
    private UserService userService;

    @GetMapping("setadmin")
    public String setAdmin(@AuthenticationPrincipal CustomUserDetails userDetails, RedirectAttributes redirAttrs) {
        String username = userDetails.getUsername();
        User user = userService.getUserByName(username);
        userService.setUserAdmin(user.getId());
        redirAttrs.addFlashAttribute("success", "Vous êtes désormais admin");
        return "redirect:/";
    }

    @GetMapping("/admin")
    public String home(@AuthenticationPrincipal CustomUserDetails userDetails, RedirectAttributes redirAttrs, Model model) {
        String username = userDetails.getUsername();
        User user = userService.getUserByName(username);
        model.addAttribute("user", user);

        if (!user.isAdmin()) {
            redirAttrs.addFlashAttribute("error", "Vous ne disposez pas des privilèges nécéssaires pour accéder à cette page");
            return "redirect:/";
        }

        return "admin";
    }
}
