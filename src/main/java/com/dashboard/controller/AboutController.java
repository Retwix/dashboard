package com.dashboard.controller;

import com.dashboard.entity.EntityService;
import com.dashboard.entity.Widget;
import com.dashboard.service.ServiceService;
import com.dashboard.service.WidgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
public class AboutController {

    @Autowired
    WidgetService widgetService;

    @Autowired
    ServiceService serviceService;

    @GetMapping("about.json")
    public HashMap<Object, Object> getAboutJson() {
        HashMap<Object, Object> map = new HashMap<>();
        ArrayList<Object> servicesList = getServices();

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                .getRequest();

        String ip = request.getRemoteAddr();

        HashMap<String, String> host = new HashMap<>();
        HashMap<String, Object> server = new HashMap<>();

        host.put("host", ip);
        server.put("current_time", Long.toString(Instant.now().getEpochSecond()));

        map.put("client", host);
        map.put("server", server);
        map.put("services", servicesList);

        return map;
    }

    private ArrayList<Object> getParams() {
        HashMap<Object, Object> params = new HashMap<>();
        ArrayList<Object> paramsList = new ArrayList<>();
        params.put("name", "serviceId");
        params.put("type", "integer");
        paramsList.add(params);
        params = new HashMap<>();
        params.put("name", "config");
        params.put("type", "string");
        paramsList.add(params);
        params = new HashMap<>();
        params.put("name", "typeApiRequest");
        params.put("type", "string");
        paramsList.add(params);
        params = new HashMap<>();
        params.put("name", "urlApiRequest");
        params.put("type", "string");
        paramsList.add(params);
        params = new HashMap<>();
        params.put("name", "page");
        params.put("type", "string");
        paramsList.add(params);
        return paramsList;
    }

    private ArrayList<Object> getServices() {
        HashMap<Object, Object> services = new HashMap<>();
        HashMap<Object, Object> widgets = new HashMap<>();
        ArrayList<Object> paramsList = getParams();
        ArrayList<Object> widgetsList = new ArrayList<>();
        ArrayList<Object> servicesList = new ArrayList<>();

        for (EntityService service : serviceService.getAllServices()) {
            services.put("name", service.getName());
            List<Widget> widgetsLst = widgetService.getAllWidgetsFromServiceId(service.getId());
            for (Widget widget: widgetsLst) {
                widgets.put("name", widget.getName());
                widgets.put("params", paramsList);
                widgetsList.add(widgets);
                widgets = new HashMap<>();
            }
            services.put("widgets", widgetsList);
            widgetsList = new ArrayList<>();
            servicesList.add(services);
            services = new HashMap<>();
        }
        return servicesList;
    }
}
