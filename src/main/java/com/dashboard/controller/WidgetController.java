package com.dashboard.controller;

import com.dashboard.entity.Widget;
import com.dashboard.service.WidgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class WidgetController {

    @Autowired
    private WidgetService widgetService;

    @GetMapping("widget/delete/{id}")
    public void deleteWidget(@PathVariable int id) {
        widgetService.deleteWidgetById(id);
    }

    @GetMapping("widgets/list")
    public List<Widget> getAllWidgets() {
        return widgetService.getAllWidgets();
    }

    @PostMapping("widget/create")
    public Widget createWidget(Model model, Widget widget) {
        model.addAttribute("widget", widget);

        Widget widgetCmp = widgetService.findUrlApiRequestByServiceId(widget.getServiceId(), widget.getUrlApiRequest());

        if (widgetCmp != null) {
            return new Widget();
        }

        String apiRequest = widget.getTypeApiRequest();

        if (!"GET".equals(apiRequest) && !"POST".equals(apiRequest)) {
            return new Widget();
        }
        widgetService.save(widget);

        return widget;
    }
}
