package com.dashboard.controller;

import com.dashboard.entity.GitLabUser;
import com.dashboard.entity.User;
import com.dashboard.security.CustomUserDetails;
import com.dashboard.service.ApiService;
import com.dashboard.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private ApiService apiService;

    @GetMapping("/login")
    public String loginView(Model model) {
        model.addAttribute("user", new User());
        return "login";
    }

    @GetMapping("/update")
    public String updateView(@AuthenticationPrincipal CustomUserDetails userDetails, Model model) {
        String username = userDetails.getUsername();
        User user = userService.getUserByName(username);
        model.addAttribute("user", user);
        return "update";
    }
  
    @GetMapping("/login_with_gitlab")
    public String loginWithGitlab(Model model, User user, String password) {
        user.setPassword(password);
        model.addAttribute("user", user);
        return "login_with_gitlab";
    }

    @GetMapping("/connect-gitlab")
    public String connectGitlab(HttpServletRequest request)
    {
        return "redirect_gitlab";
    }

    @GetMapping("/get-gitlab-token")
    public String getGitlabToken(@RequestParam String access_token, Model model) throws JsonProcessingException {
        User user = new User();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED.toString());
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        headers.add("Authorization", "Bearer " + access_token);

        ResponseEntity<String> response = apiService.apiRequest("https://gitlab.com/api/v4/user/", "GET", headers, null);

        return connectUserFromGitLab(response.getBody(), user, model);
    }

    private String connectUserFromGitLab(String json, User user, Model model) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<GitLabUser> typeReference = new TypeReference<GitLabUser>(){};
        GitLabUser gitLabUser = mapper.readValue(json, typeReference);

        User usernameCompared = userService.getUserByName(gitLabUser.getUsername());
        User emailCompared = userService.getUserByEmail(gitLabUser.getEmail());

        if (usernameCompared != null && emailCompared != null) {
            model.addAttribute("user", usernameCompared);
            return loginWithGitlab(model, usernameCompared, gitLabUser.getId().toString());
        }

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode(gitLabUser.getId().toString());
        user.setPassword(encodedPassword);

        user.setAvatar("https://avatars.dicebear.com/api/gridy/" + gitLabUser.getUsername() + ".svg");

        user.setUsername(gitLabUser.getUsername());
        user.setEmail(gitLabUser.getEmail());

        userService.saveUser(user);

        model.addAttribute("user", user);

        return loginWithGitlab(model, user, gitLabUser.getId().toString());
    }

    @PostMapping("/register")
    public String register(@ModelAttribute User user, Model model, RedirectAttributes redirAttrs) {
        model.addAttribute("user", user);

        User usernameCompared = userService.getUserByName(user.getUsername());
        User emailCompared = userService.getUserByEmail(user.getEmail());

        if (emailCompared != null) {
            redirAttrs.addFlashAttribute("error", "Email déjà utilisée");
            return "redirect:/login";
        }
        if (usernameCompared != null) {
            redirAttrs.addFlashAttribute("error", "Pseudo déjà utilisé");
            return "redirect:/login";
        }

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode(user.getPassword());
        user.setPassword(encodedPassword);

        user.setAvatar("https://avatars.dicebear.com/api/gridy/" + user.getUsername() + ".svg");

        userService.saveUser(user);
        return "redirect:/login?success";
    }

    @PostMapping("/update")
    public String update(@AuthenticationPrincipal CustomUserDetails userDetails, @ModelAttribute User user, Model model, RedirectAttributes redirAttrs) {
        model.addAttribute("user", user);
        String username = userDetails.getUsername();
        User userCmp = userService.getUserByName(username);

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode(user.getPassword());
        user.setPassword(encodedPassword);

        System.out.println(encodedPassword);
        System.out.println(userCmp.getPassword());

        if (encodedPassword.equals(userCmp.getPassword())) {
            redirAttrs.addFlashAttribute("error", "Le mot de passe ne peut pas être identique au précédent");
            return "redirect:/update";
        }

        userService.saveUser(user);
        redirAttrs.addFlashAttribute("success", "Le mot de passe a été mis à jour");
        return "redirect:/update";
    }
}
