package com.dashboard.controller;

import com.dashboard.entity.EntityService;
import com.dashboard.entity.Widget;
import com.dashboard.service.ServiceService;
import com.dashboard.service.WidgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@RestController
public class ServiceController {
    @Autowired
    private ServiceService serviceService;

    @Autowired
    private WidgetService widgetService;

    @GetMapping("/service/list")
    public List<EntityService> getAllServices() {
        return serviceService.getAllServices();
    }

    @GetMapping("/service/{serviceId}/widgets")
    public List<Widget> getAllWidgetsFromServiceId(@PathVariable int serviceId) {
        return widgetService.getAllWidgetsFromServiceId(serviceId);
    }

    @GetMapping("/service/delete/{id}")
    public void deleteServiceAndItsWidgetsById(@PathVariable int id) {
        serviceService.deleteServiceById(id);
        widgetService.deleteWidgetsByServiceId(id);
    }

    @PostMapping("/service/create")
    public EntityService createService(Model model, EntityService service, RedirectAttributes redirAttrs) {
        model.addAttribute("service", service);

        EntityService serviceCmp = serviceService.findByName(service.getName());

        if (serviceCmp != null) {
            return new EntityService();
        }

        serviceService.save(service);

        return service;
    }
}
