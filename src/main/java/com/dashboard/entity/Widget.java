package com.dashboard.entity;

import com.sun.istack.NotNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="widgets")
public class Widget {
    @Id
    @GeneratedValue
    private int id;

    private int serviceId;

    private String config;

    @NotNull
    private String typeApiRequest;

    @NotNull
    private String urlApiRequest;

    @NotNull
    private String name;

    @NotNull
    private String page;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String option) {
        this.config = option;
    }

    public String getTypeApiRequest() {
        return typeApiRequest;
    }

    public void setTypeApiRequest(String typeApiRequest) {
        this.typeApiRequest = typeApiRequest;
    }

    public String getUrlApiRequest() {
        return urlApiRequest;
    }

    public void setUrlApiRequest(String urlApiRequest) {
        this.urlApiRequest = urlApiRequest;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
