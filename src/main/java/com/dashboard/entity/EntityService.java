package com.dashboard.entity;

import com.sun.istack.NotNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="services")
public class EntityService {
    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private String connectUrl;

    @NotNull
    private String name;

    @NotNull
    private String icon;

    @NotNull
    private String defaultWebsite;

    private String description;

    @NotNull
    private String pageReference;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getConnectUrl() {
        return connectUrl;
    }

    public void setConnectUrl(String connectUrl) {
        this.connectUrl = connectUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDefaultWebsite() {
        return defaultWebsite;
    }

    public void setDefaultWebsite(String defaultWebsite) {
        this.defaultWebsite = defaultWebsite;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPageReference() {
        return pageReference;
    }

    public void setPageReference(String pageReference) {
        this.pageReference = pageReference;
    }
}
