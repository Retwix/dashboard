package com.dashboard.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@Entity
public class GitLabUser {
    @Id
    private Long id;

    private String name;

    private String username;

    @JsonIgnore
    private String state;

    @JsonIgnore
    private String avatar_url;

    @JsonIgnore
    private String web_url;

    @JsonIgnore
    private String created_at;

    @JsonIgnore
    private String bio;

    @JsonIgnore
    private String bio_html;

    @JsonIgnore
    private String location;

    @JsonIgnore
    private String public_email;

    @JsonIgnore
    private String skype;

    @JsonIgnore
    private String linkedin;

    @JsonIgnore
    private String twitter;

    @JsonIgnore
    private String website_url;

    @JsonIgnore
    private String organization;

    @JsonIgnore
    private String job_title;

    @JsonIgnore
    private String work_information;

    @JsonIgnore
    private String last_sign_in_at;

    @JsonIgnore
    private String confirmed_at;

    @JsonIgnore
    private String last_activity_on;

    private String email;

    @JsonIgnore
    private String theme_id;

    @JsonIgnore
    private String color_scheme_id;

    @JsonIgnore
    private String projects_limit;

    @JsonIgnore
    private String current_sign_in_at;

    @JsonIgnore
    private String identities;

    @JsonIgnore
    private String can_create_group;

    @JsonIgnore
    private String can_create_project;

    @JsonIgnore
    private String two_factor_enabled;

    @JsonIgnore
    private String external;

    @JsonIgnore
    private String private_profile;

    @JsonIgnore
    private String shared_runners_minutes_limit;

    @JsonIgnore
    private String extra_shared_runners_minutes_limit;

    public GitLabUser() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
