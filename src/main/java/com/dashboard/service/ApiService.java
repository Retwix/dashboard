package com.dashboard.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class ApiService {

    public ResponseEntity<String> apiRequest(String url, String type, HttpHeaders headers, MultiValueMap<String, String> formData) {
        RestTemplate restTemplate = new RestTemplate();

        HttpMethod method = HttpMethod.GET;
        if ("GET".equals(type)) {
            method = HttpMethod.GET;
        }
        if ("POST".equals(type)) {
            method = HttpMethod.GET;
        }

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(formData, headers);
        try {
            return restTemplate.exchange(url, method, request, String.class);
        } catch(final HttpClientErrorException e) {
            System.out.println(e.getStatusCode());
            return null;
        }
    }
}
