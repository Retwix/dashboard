package com.dashboard.service;

import com.dashboard.entity.Widget;
import com.dashboard.repository.WidgetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WidgetService {

    @Autowired
    private WidgetRepository widgetRepository;

    public Widget save(Widget widget) {
        return widgetRepository.save(widget);
    }

    public Widget findUrlApiRequestByServiceId(int serviceId, String urlApiRequest) {
        return widgetRepository.findUrlApiRequestByServiceId(serviceId, urlApiRequest);
    }

    public List<Widget> getAllWidgetsFromServiceId(int serviceId) {
        return widgetRepository.getAllWidgetsFromServiceId(serviceId);
    }

    public void deleteWidgetById(int id) {
        widgetRepository.deleteById(id);
    }

    public void deleteWidgetsByServiceId(int serviceId) {
        widgetRepository.deleteWidgetsByServiceId(serviceId);
    }

    public List<Widget> getAllWidgets() {
        return widgetRepository.findAll();
    }
}
