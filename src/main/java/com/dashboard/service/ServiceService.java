package com.dashboard.service;

import com.dashboard.entity.EntityService;
import com.dashboard.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceService {

    @Autowired
    private ServiceRepository serviceRepository;

    public EntityService save(EntityService service) {
        return serviceRepository.save(service);
    }

    public EntityService findByName(String name) {
        return serviceRepository.findByName(name);
    }

    public List<EntityService> getAllServices() {
        return serviceRepository.findAll();
    }

    public void deleteServiceById(int id) {
        serviceRepository.deleteById(id);
    }
}
