package com.dashboard.service;

import com.dashboard.entity.User;
import com.dashboard.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public User getUserByName(String username) {
        return userRepository.findByUsername(username);
    }

    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User updatePasswordUserById(int id, String password) {
        return userRepository.updatePasswordUserById(id, password);
    }

    public void setUserAdmin(int id) {
        userRepository.setUserAdmin(id);
    }
}
