package com.dashboard.repository;

import com.dashboard.entity.EntityService;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceRepository extends JpaRepository<EntityService, Integer> {

    EntityService findByName(String name);
}
