package com.dashboard.repository;

import com.dashboard.entity.Widget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface WidgetRepository extends JpaRepository<Widget, Integer> {

    @Query("SELECT w FROM Widget w WHERE w.serviceId = ?1 AND w.urlApiRequest = ?2")
    Widget findUrlApiRequestByServiceId(int serviceId, String urlApiRequest);

    @Query(value = "SELECT * FROM widgets w WHERE w.service_id = ?1", nativeQuery = true)
    List<Widget> getAllWidgetsFromServiceId(int serviceId);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM Widget w WHERE w.serviceId = ?1")
    void deleteWidgetsByServiceId(int serviceId);
}
