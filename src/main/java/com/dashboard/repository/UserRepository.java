package com.dashboard.repository;

import com.dashboard.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsername(String username);

    User findByEmail(String email);

    @Query("UPDATE User u SET u.password = ?2 WHERE u.id = ?1")
    User updatePasswordUserById(int id, String password);

    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.admin = true")
    void setUserAdmin(int id);
}
