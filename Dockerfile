FROM openjdk:15 AS TEMP_BUILD_IMAGE
ENV APP_HOME=/usr/app/
WORKDIR $APP_HOME
COPY . .
RUN ./gradlew build

FROM openjdk:15-oraclelinux7
RUN yum install -y nc
ENV ARTIFACT_NAME=dashboard-0.0.1-SNAPSHOT.jar
ENV APP_HOME=/usr/app/
WORKDIR $APP_HOME
COPY --from=TEMP_BUILD_IMAGE $APP_HOME/build/libs/$ARTIFACT_NAME .
COPY ./entry_point.sh .
EXPOSE 8080
RUN chmod 755 entry_point.sh 
CMD ./entry_point.sh
